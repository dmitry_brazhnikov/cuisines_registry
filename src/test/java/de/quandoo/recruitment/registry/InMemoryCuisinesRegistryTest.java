package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class InMemoryCuisinesRegistryTest {

    private CuisinesRegistry cuisinesRegistry;

    private final static String[] TEST_CUSTOMERS_IDS = {
            "a9b0831d-1b9d-4ba7-a285-f03e94ebb56b",
            "6a587ebe-c483-43b7-b08e-521ada2e6a63",
            "bd13bfe4-a5b7-48c0-90fd-bdd7f61f4220",
            "21b6d7ad-10d5-45a7-87b5-f27a812297fd",
            "2af98a73-690b-4b34-8384-b65fedf5e5e0",
            "6d607c61-bfc6-4fa9-b1a3-f8d13343f571",
            "d7e9a1d8-bba7-458b-9409-b674d5105f24",
            "ef4f528f-742d-4346-a5c1-2522c7b999a4",
            "297ba6d7-7b5c-4a56-8107-acd94871c699",
            "9be69358-dd76-4320-9c0f-b6e0e44cfe81"};

    private final static String[] TEST_CUISINES_NAMES = {
            "Austrian","Czech","German","Italian","Hungarian","Polish",
            "Liechtensteiner","Slovak","Slovenian","Swiss","British",
            "Irish", "Latvian","Lithuanian","Norwegian"};

    private Cuisine austrianCuisine;
    private Customer germanCustomer;;
    private Customer italianCustomer;

    @Before
    public void setUp() {
        cuisinesRegistry = new InMemoryCuisinesRegistry();

        austrianCuisine = new Cuisine(TEST_CUISINES_NAMES[0]);
        cuisinesRegistry.register(new Customer(TEST_CUSTOMERS_IDS[0]), austrianCuisine);
        cuisinesRegistry.register(new Customer(TEST_CUSTOMERS_IDS[0]), new Cuisine(TEST_CUISINES_NAMES[0]));
        cuisinesRegistry.register(new Customer(TEST_CUSTOMERS_IDS[0]), new Cuisine(TEST_CUISINES_NAMES[0]));

        germanCustomer = new Customer(TEST_CUSTOMERS_IDS[1]);
        cuisinesRegistry.register(new Customer(TEST_CUSTOMERS_IDS[1]), new Cuisine(TEST_CUISINES_NAMES[0]));
        cuisinesRegistry.register(new Customer(TEST_CUSTOMERS_IDS[1]), new Cuisine(TEST_CUISINES_NAMES[2]));
        cuisinesRegistry.register(new Customer(TEST_CUSTOMERS_IDS[1]), new Cuisine(TEST_CUISINES_NAMES[2]));
        cuisinesRegistry.register(new Customer(TEST_CUSTOMERS_IDS[1]), new Cuisine(TEST_CUISINES_NAMES[3]));

        italianCustomer = new Customer(TEST_CUSTOMERS_IDS[2]);
        cuisinesRegistry.register(italianCustomer, new Cuisine(TEST_CUISINES_NAMES[5]));
        cuisinesRegistry.register(italianCustomer, new Cuisine(TEST_CUISINES_NAMES[5]));
        cuisinesRegistry.register(italianCustomer, new Cuisine(TEST_CUISINES_NAMES[6]));
        cuisinesRegistry.register(new Customer(TEST_CUSTOMERS_IDS[2]), new Cuisine(TEST_CUISINES_NAMES[3]));
    }

    @Test
    public void testGetCustomers_whenSameCustomerAndSameCuisineAdded_returnsUniqueCustomerAndCuisines() {
        List<Customer> austrianCuisineLovers = cuisinesRegistry.cuisineCustomers(austrianCuisine);
        assertSame(2, austrianCuisineLovers.size());
        assertTrue(austrianCuisineLovers.contains(germanCustomer));
        assertTrue(austrianCuisineLovers.contains(new Customer(TEST_CUSTOMERS_IDS[0])));
    }

    @Test
    public void testGetCuisines_whenSameCustomerAndSameCuisineAdded_returnsUniqueCustomerAndCuisines() {
        List<Cuisine> germanCustomerCuisines = cuisinesRegistry.customerCuisines(germanCustomer);
        assertSame(3, germanCustomerCuisines.size());
        assertTrue(germanCustomerCuisines.contains(austrianCuisine));
        assertTrue(germanCustomerCuisines.contains(new Cuisine(TEST_CUISINES_NAMES[2])));
        assertTrue(germanCustomerCuisines.contains(new Cuisine(TEST_CUISINES_NAMES[3])));
    }


    /**
     * Following three tests remained to support compatibility with earlier version of Registry
     * Bad practice to return value for illegal argument if we know exactly that such elements cannot be stored in registry
     * TODO: eliminated usages with passing null as parameter
     */
    @Test
    public void shouldWork1() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));

        cuisinesRegistry.cuisineCustomers(new Cuisine("french"));
    }

    @Test
    public void shouldWork2() {
        cuisinesRegistry.cuisineCustomers(null);
    }

    @Test
    public void shouldWork3() {
        cuisinesRegistry.customerCuisines(null);
    }

    /**
     * Ignored as method is now implemented
     * TODO: remove test
     */
    @Ignore
    @Test(expected = RuntimeException.class)
    public void thisDoesntWorkYet() {
        cuisinesRegistry.topCuisines(1);
    }


}