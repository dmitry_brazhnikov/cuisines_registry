package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CuisineRankingTest {

    private CuisinesRegistry registry;

    private final static String[] TEST_CUSTOMERS_IDS = {
            "a9b0831d-1b9d-4ba7-a285-f03e94ebb56b",
            "6a587ebe-c483-43b7-b08e-521ada2e6a63",
            "bd13bfe4-a5b7-48c0-90fd-bdd7f61f4220",
            "21b6d7ad-10d5-45a7-87b5-f27a812297fd",
            "2af98a73-690b-4b34-8384-b65fedf5e5e0",
            "6d607c61-bfc6-4fa9-b1a3-f8d13343f571",
            "d7e9a1d8-bba7-458b-9409-b674d5105f24",
            "ef4f528f-742d-4346-a5c1-2522c7b999a4",
            "297ba6d7-7b5c-4a56-8107-acd94871c699",
            "9be69358-dd76-4320-9c0f-b6e0e44cfe81"};

    private final static String[] TEST_CUISINES_NAMES = {
            "Austrian","Czech","German","Italian","Hungarian","Polish",
            "Liechtensteiner","Slovak","Slovenian","Swiss","British",
            "Irish", "Latvian","Lithuanian","Norwegian"};

    @Before
    public void setUp() {
        registry = new InMemoryCuisinesRegistry();
    }

    @Test
    public void testGetTopCuisines_whenCustomerToCuisineOneToOne_returnsListOfUniquesCuisines() {
        List<Customer> customers = new ArrayList<>();

        Customer customerWithOneCuisine = new Customer(TEST_CUSTOMERS_IDS[0]);
        registry.register(customerWithOneCuisine, new Cuisine(TEST_CUISINES_NAMES[0]));

        Customer oneCustomerWithTwoCuisines = new Customer(TEST_CUSTOMERS_IDS[1]);
        registry.register(customerWithOneCuisine, new Cuisine(TEST_CUISINES_NAMES[1]));

        Customer anotherCustomerWithTwoCuisines = new Customer(TEST_CUSTOMERS_IDS[2]);
        registry.register(customerWithOneCuisine, new Cuisine(TEST_CUISINES_NAMES[2]));

        Customer customerWithFourCuisines = new Customer(TEST_CUSTOMERS_IDS[3]);
        registry.register(customerWithOneCuisine, new Cuisine(TEST_CUISINES_NAMES[3]));

        List<Cuisine> topCuisines = registry.topCuisines(4);

        assertEquals("Result should have 4 cuisines, but has " + topCuisines.size(),
                4, topCuisines.size());
        assertTrue(topCuisines.contains(new Cuisine(TEST_CUISINES_NAMES[0])));
        assertTrue(topCuisines.contains(new Cuisine(TEST_CUISINES_NAMES[1])));
        assertTrue(topCuisines.contains(new Cuisine(TEST_CUISINES_NAMES[2])));
        assertTrue(topCuisines.contains(new Cuisine(TEST_CUISINES_NAMES[3])));

    }

    @Test
    public void testGetTopCuisines_whenCustomerToCuisineOneToMany_returnsSortedList() {
        List<Customer> customers = new ArrayList<>();

        Customer customerWithOneCuisine = new Customer(TEST_CUSTOMERS_IDS[0]);
        registry.register(customerWithOneCuisine, new Cuisine(TEST_CUISINES_NAMES[0]));

        Customer customerWithTwoCuisines = new Customer(TEST_CUSTOMERS_IDS[1]);
        registry.register(customerWithOneCuisine, new Cuisine(TEST_CUISINES_NAMES[0]));
        registry.register(customerWithOneCuisine, new Cuisine(TEST_CUISINES_NAMES[9]));

        Customer customerWithThreeCuisines = new Customer(TEST_CUSTOMERS_IDS[2]);
        registry.register(customerWithThreeCuisines, new Cuisine(TEST_CUISINES_NAMES[0]));
        registry.register(customerWithThreeCuisines, new Cuisine(TEST_CUISINES_NAMES[9]));
        registry.register(customerWithThreeCuisines, new Cuisine(TEST_CUISINES_NAMES[2]));

        Customer customerWithFourCuisines = new Customer(TEST_CUSTOMERS_IDS[3]);
        registry.register(customerWithFourCuisines, new Cuisine(TEST_CUISINES_NAMES[0]));
        registry.register(customerWithFourCuisines, new Cuisine(TEST_CUISINES_NAMES[9]));
        registry.register(customerWithFourCuisines, new Cuisine(TEST_CUISINES_NAMES[2]));
        registry.register(customerWithFourCuisines, new Cuisine(TEST_CUISINES_NAMES[3]));

        int topN = 4;
        List<Cuisine> topCuisines = registry.topCuisines(topN);

        assertEquals(topCuisines.size(), topN);


        assertEquals("Expected 1st cuisine is " + TEST_CUISINES_NAMES[0] + " but actual is " +
                topCuisines.get(0).getName(), new Cuisine(TEST_CUISINES_NAMES[0]), topCuisines.get(0));
        assertEquals("Expected 2nd cuisine is " + TEST_CUISINES_NAMES[9] + " but actual is " +
                topCuisines.get(1).getName(), new Cuisine(TEST_CUISINES_NAMES[9]), topCuisines.get(1));
        assertEquals("Expected 3rd cuisine is " + TEST_CUISINES_NAMES[2] + " but actual is " +
                topCuisines.get(2).getName(), new Cuisine(TEST_CUISINES_NAMES[2]), topCuisines.get(2));
        assertEquals("Expected 4th cuisine is " + TEST_CUISINES_NAMES[3] + " but actual is " +
                topCuisines.get(3).getName(), new Cuisine(TEST_CUISINES_NAMES[3]), topCuisines.get(3));
    }

    @Test(expected = RuntimeException.class)
    public void testGetTopCuisines_whenRequestedZeroTopCuisines_throwsException() {
        registry.topCuisines(0);
    }

    @Test
    public void testGetTopCuisines_whenTopCuisinesLessThanRequested_returnsList() {
        registry.register(new Customer(TEST_CUSTOMERS_IDS[0]), new Cuisine(TEST_CUISINES_NAMES[1]));
        registry.register(new Customer(TEST_CUSTOMERS_IDS[1]), new Cuisine(TEST_CUISINES_NAMES[2]));
        registry.register(new Customer(TEST_CUSTOMERS_IDS[2]), new Cuisine(TEST_CUISINES_NAMES[3]));
        registry.register(new Customer(TEST_CUSTOMERS_IDS[3]), new Cuisine(TEST_CUISINES_NAMES[4]));
        registry.register(new Customer(TEST_CUSTOMERS_IDS[4]), new Cuisine(TEST_CUISINES_NAMES[5]));
        registry.register(new Customer(TEST_CUSTOMERS_IDS[5]), new Cuisine(TEST_CUISINES_NAMES[6]));

        List<Cuisine> top10Cuisines = registry.topCuisines(10);
        assertSame(6, top10Cuisines.size());
    }

}
