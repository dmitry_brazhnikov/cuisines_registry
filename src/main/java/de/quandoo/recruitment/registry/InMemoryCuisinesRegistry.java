package de.quandoo.recruitment.registry;

import com.google.common.base.Preconditions;
import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.*;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author Dmitry Brazhnikov
 * InMemory thread-safe implementation of {@link de.quandoo.recruitment.registry.api.CuisinesRegistry}
 */
public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private final Map<Customer, List<Cuisine>> customerToCuisinesMap = new HashMap<>();
    private final Map<Cuisine, List<Customer>> cuisineToCustomersMap = new HashMap<>();

    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    @Override
    public void register(final Customer customer, final Cuisine cuisine) {
        Preconditions.checkNotNull(customer, "Customer should not be null");
        Preconditions.checkNotNull(customer, "Cuisine should not be null");

        register(customer, cuisine, customerToCuisinesMap, cuisineToCustomersMap);
    }

    private void register(Customer customer, Cuisine cuisine,
                           Map<Customer, List<Cuisine>> customerToCuisinesMap,
                           Map<Cuisine, List<Customer>> cuisineToCustomersMap) {
        lock.writeLock().lock();
        try {
            addValueToMappedList(customer, cuisine, customerToCuisinesMap);
            addValueToMappedList(cuisine, customer, cuisineToCustomersMap);
        } finally {
            lock.writeLock().unlock();
        }
    }

    private <K,V> void addValueToMappedList(K key, V value, Map<K, List<V>> mapOfLists) {
        List<V> values = mapOfLists.get(key);
        if (values == null) {
            values = new LinkedList<>();
        }
        if (!values.contains(value)) {
            values.add(value);
        }
        mapOfLists.put(key, values);
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        if (cuisine == null) {
            return Collections.emptyList();
        } else {
            lock.readLock().lock();
            try {
                return cuisineToCustomersMap.get(cuisine);
            } finally {
                lock.readLock().unlock();
            }
        }
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        if (customer == null ) {
            return Collections.emptyList();
        } else {
            lock.readLock().lock();
            try {
                return customerToCuisinesMap.get(customer);
            } finally {
                lock.readLock().unlock();
            }
        }
    }

    @Override
    public List<Cuisine> topCuisines(final int numberOfTopCuisines) {
        Preconditions.checkState(numberOfTopCuisines > 0,
                "Requested number of top categories should be more than zero, " +
                        "but it is " + numberOfTopCuisines);
        List<Cuisine> topCuisines = new ArrayList<>(numberOfTopCuisines);

        lock.readLock().lock();
        try {
            List<Map.Entry<Cuisine, List<Customer>>> entries = new LinkedList<>(cuisineToCustomersMap.entrySet());
            Collections.sort(entries, new Comparator<Map.Entry<Cuisine, List<Customer>>>() {
                @Override
                public int compare(Map.Entry<Cuisine, List<Customer>> lhsCuisineCustomers,
                                   Map.Entry<Cuisine, List<Customer>> rhsCuisineCustomers) {
                    return rhsCuisineCustomers.getValue().size() - lhsCuisineCustomers.getValue().size();
                }
            });

            for (int i = 0; i < numberOfTopCuisines && i < entries.size(); i++) {
                topCuisines.add(entries.get(i).getKey());
            }
        } finally {
            lock.readLock().unlock();
        }
        return topCuisines;
    }
}
